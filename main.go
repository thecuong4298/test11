package main

import (
	"github.com/jlaffaye/ftp"
	"log"
	"time"
)

func main() {
	c, err := ftp.Dial("localhost:21", ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	err = c.Login("CLIPZONE", "CL1PZONE#abc")
	if err != nil {
		log.Fatal(err)
	}

	lst, _ := c.List("/ftp/cdr")

	println(lst)
	// Do something with the FTP conn

	if err := c.Quit(); err != nil {
		log.Fatal(err)
	}
}
